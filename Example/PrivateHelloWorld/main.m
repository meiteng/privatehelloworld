//
//  main.m
//  PrivateHelloWorld
//
//  Created by meiteng on 03/07/2021.
//  Copyright (c) 2021 meiteng. All rights reserved.
//

@import UIKit;
#import "BYAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BYAppDelegate class]));
    }
}
