//
//  BYAppDelegate.h
//  PrivateHelloWorld
//
//  Created by meiteng on 03/07/2021.
//  Copyright (c) 2021 meiteng. All rights reserved.
//

@import UIKit;

@interface BYAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
